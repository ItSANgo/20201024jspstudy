package net.obs_cop.show_hidden;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.obs_cop.bean.NewFileBean;
import net.obs_cop.database.DBAccess;

/**
 * Servlet implementation class ShowHidden
 */
@WebServlet("/ShowHidden")
public class ShowHidden extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final String Hidden = "/WEB-INF/NewFile.jsp";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowHidden() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        System.out.println("doGet start");

		// 2021/03/27 ADD S.Namba DB接続サンプルコード Start
		DBAccess da = new DBAccess();
		try {
			List<Map<String, Object>> aDto = da.select("select * from sample_a where id = ?", Arrays.asList("1"));

			for (Map<String, Object> dto: aDto) {
				System.out.println(dto.get("ID"));
				System.out.println(dto.get("MEMO"));
				System.out.println(dto.get("UPDATETIME"));
			}

			int res = da.update("update sample_a set memo = ? where id = ?", Arrays.asList("hogehoge", "1"));

			System.out.println(res);


			NewFileBean bean = new NewFileBean();
			bean.setXxx("Mike");
			request.setAttribute("Bean",bean);

	        RequestDispatcher dispacher = request.getRequestDispatcher(Hidden);
	        dispacher.forward(request, response);

		} finally {
			da.close();
		}
		// 2021/03/27 ADD S.Namba DB接続サンプルコード End

		System.out.println("doGet end");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
        request.setCharacterEncoding("UTF-8");

		System.out.println("doPost start");

		String xxx = request.getParameter("test").toString();
		NewFileBean bean = new NewFileBean();
		bean.setXxx(xxx);
		request.setAttribute("Bean",bean);

		doGet(request, response);

		System.out.println("doPost end");
	}

}
