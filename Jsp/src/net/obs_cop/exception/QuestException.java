package net.obs_cop.exception;

public class QuestException extends RuntimeException {
	public QuestException(String msg, Throwable e) {
		super(msg, e);
	}
	public QuestException(String msg) {
		super(msg);
	}
}
