package net.obs_cop.database;

import java.math.BigDecimal;

// メモ
//難波
//①updateを作る(insert deleteも)
//③update/insert/deleteのサンプルを作る

//森長
//②selectのサンプルを作る → DBdebug.javaに少しある
//⑤画面のサンプルを作る

//金澤
//④トランザクション制御を組み込む

// 今後
//⑥接続文字列等をpropertyファイルかxmlファイルから読み取る
//⑦ログ出力
//⑧共通の例外クラス

// C:\pleiades\tomcat\8\lib
// mariadb-java-client-2.4.0.jarを格納する
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DBAccess implements AutoCloseable {

	private Connection conn;
	private String url = "jdbc:mariadb://localhost/obs2020";
	private String id = "obs";
	private String pw = "obs2020";

	public DBAccess() {
		connect();
	}

	private void connect() {

		// jdbcドライバ確認
		try {
			Class.forName("org.mariadb.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("MariaDB JDBC Driver was not found", e);
		}

		// 接続
		try {
			conn = DriverManager.getConnection(url, id, pw);
			conn.setAutoCommit(false);
		} catch (SQLException e) {
			throw new RuntimeException("Couldn't connect DB", e);
		}
	}

	public void commit() {
		try {
			conn.commit();
		} catch (SQLException e) {
			throw new RuntimeException("Couldn't commit", e);
		}
	}

	public void rollback() {
		try {
			conn.rollback();
		} catch (SQLException e) {
			throw new RuntimeException("Couldn't rollback", e);
		}
	}

	/**
	 * 登録
	 *
	 * @param sql
	 * @param parameters
	 * @return
	 */
	public int insert(String sql, List<String> parameters) {
		return execute(sql,parameters);
	}

	/**
	 * 更新
	 *
	 * @param sql
	 * @param parameters
	 * @return
	 */
	public int update(String sql, List<String> parameters) {
		return execute(sql, parameters);
	}

	/**
	 * 削除
	 *
	 * @param sql
	 * @param parameters
	 * @return
	 */
	public int delete(String sql, List<String> parameters) {
		return execute(sql,parameters);
	}

	/**
	 * DML実行
	 *
	 * @param sql
	 * @param parameters
	 * @return
	 */
	private int execute(String sql, List<String> parameters) {
		PreparedStatement stmt = null;
		int res = 0;
		try {
			stmt = conn.prepareStatement(sql);

			if (parameters != null) {
				for (int i = 0; i < parameters.size(); i++) {
					stmt.setString(i + 1, parameters.get(i));
				}
			}
			res = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					System.out.println("stmt close Exception");
					e.printStackTrace();
				}
			}
		}

		return res;
	}

	/**
	 * 行ロック
	 *
	 * @param sql
	 * @param parameters
	 * @return
	 */
	public List<Map<String, Object>> selectForUpdate(String sql, List<Object> parameters) {
		sql = sql + " FOR UPDATE";
		return select(sql, parameters);
	}

	// 2021/03/27 ADD S.Namba DB検索サンプルコード Start
	public List<Map<String, Object>> select(String sql, List<Object> parameters) {
		PreparedStatement stmt = null;
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			stmt = conn.prepareStatement(sql);

			if (parameters != null) {
				for (int i = 0; i < parameters.size(); i++) {
					if(parameters.get(i) instanceof String) {
						stmt.setString(i + 1, (String)parameters.get(i));
					} else if(parameters.get(i) instanceof BigDecimal) {
						stmt.setBigDecimal(i + 1, (BigDecimal)parameters.get(i));
					} else {
						if(parameters.get(i) == null) {
							stmt.setNull(i + 1, Types.NULL);
						} else {
							stmt.setString(i + 1, parameters.get(i).toString());
						}
				}
				}
			}
			ResultSet hrs = stmt.executeQuery();

			ResultSetMetaData rsmd = hrs.getMetaData();

			while (hrs.next()) {

				Map<String, Object> dto = new HashMap<>();
				list.add(dto);

				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					//フィールド名
					String field = rsmd.getColumnName(i);
					dto.put(field, hrs.getString(field));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					System.out.println("stmt close Exception");
					e.printStackTrace();
				}
			}
		}

		return list;
	}
	// 2021/03/27 ADD S.Namba DB検索サンプルコード End

	public List<BaseDto> select(String sql, BaseDto params) {

		// sqlから :xxx @xxx にマッチする文字列を取り出す

		String regex = ":.*\\s|@.*\\s";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(sql);
		if (m.find() ) {
			System.out.print ( " はマッチしました。" );
			do {
				// マッチした部分文字列を表示する
				System.out.print ( m.group() + " ");
			} while (m.find() ); // マッチした数だけループする
		}

		Statement stmt = null;
		List<BaseDto> list = new ArrayList<>();
		try {
			stmt = conn.createStatement();
			ResultSet hrs = stmt.executeQuery(sql);

			ResultSetMetaData rsmd = hrs.getMetaData();

			while (hrs.next()) {

				BaseDto dto = new BaseDto();

				for (int i = 1; i <= rsmd.getColumnCount(); i++) {
					//フィールド名
					String field = rsmd.getColumnName(i);

					//フィールド名に対するデータ
					int sqltype = rsmd.getColumnType(i);
					if (sqltype == Types.CHAR
							|| sqltype == Types.VARCHAR
							|| sqltype == Types.NCHAR
							|| sqltype == Types.NVARCHAR) {
						String getdata = hrs.getString(field);
						dto.put(field, getdata);
					} else {
						String getdata = hrs.getString(field);
						dto.put(field, getdata);
					}
				}
				//1件分のデータを格納
				list.add(dto);
			}
		} catch (SQLException e) {
			try {
				rollback();
			} catch (RuntimeException ex) {
				ex.printStackTrace();
			}
			throw new RuntimeException("select Exception", e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					throw new RuntimeException("Statement close Exception", e);
				}
			}
		}

		return list;
	}

	public void close() {
		if (conn != null) {
			try {

				// rollback
				try {
					rollback();
				} catch (RuntimeException ex) {
					ex.printStackTrace();
				}

				// close
				conn.close();
			} catch (SQLException e) {
				throw new RuntimeException("Connection close Exception", e);
			}
		}
	}
}
