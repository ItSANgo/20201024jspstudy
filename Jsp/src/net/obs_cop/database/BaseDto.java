package net.obs_cop.database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class BaseDto extends HashMap<String,Object> {

	/** 更新日時 */
	public static String UPDATETIME = "UPDATETIME";

	/** 日時フォーマット */
	private static String DATE_FORMAT = "yyyyMMddHHmmssSSS";

	/** 更新日時取得 */
	public Date getUpdateTime() throws ParseException {
	    SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		return format.parse(this.get(UPDATETIME).toString());
	}

	/** 更新日時設定 */
	public void setUpdateTime(Date updateTime) {
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		this.put(UPDATETIME,format.format(updateTime));
	}

	// 更新者
	private String updateName;
	public String getUpdateName() {
		return updateName;
	}
	public void setUpdateName(String updateName) {
		this.updateName = updateName;
	}
	// 登録時間
	private Date insertTime;
	public Date getInsertTime() {
		return insertTime;
	}
	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}
	// 登録者
	private String insertName;
	public String getInsertName() {
		return insertName;
	}
	public void setInsertName(String insertName) {
		this.insertName = insertName;
	}
	// 件数
	private Integer recordCount;
	public Integer getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

//	private HashMap<String,Object>  map = new HashMap<>();
//	public void put(String key,Object value) {
//		map.put(key, value);
//	}
}
