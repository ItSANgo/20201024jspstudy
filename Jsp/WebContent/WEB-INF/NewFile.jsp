<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page import="net.obs_cop.bean.NewFileBean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<jsp:useBean id="Bean" class="net.obs_cop.bean.NewFileBean" scope="request"/>
<html>
<head>
<link rel="stylesheet" href="./css/NewFile.css">
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<form action="/Jsp/ShowHidden" Method="POST">
<main>
	<div id="rgba">成功しました!</div>
	<div class="rgba">テスト１</div>
	<p>${Bean.xxx}</p>
	<p><%=Bean.getXxx() %></p>
	<input type="text" name="test" value="テスト">
	<input type="submit" value="テスト">
	<c:if test="${Bean.xxx == 'テスト成功'}" >
		<c:out value="${Bean.xxx}" />
	</c:if>
<%
	NewFileBean a = new NewFileBean();
	a.setXxx("aaa");
%>
<p><%=a.getXxx() %></p>
</main>
</form>
</body>
</html>
