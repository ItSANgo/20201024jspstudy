# 第7回勉強会 Java<->Maria DB接続

## Maria DB JDBC client driverを探す

### Maven Reositoryを検索

 <https://mvnrepository.com/artifact/org.mariadb.jdbc/mariadb-java-client>
2.7.2が2021-02-20時点の最新であることが判る。
2.7.2をclick->Home Pageが判る。
 <https://mariadb.com/kb/en/about-mariadb-connector-j/>

### 「Download MariaDB Connector/J」をclick

 <https://mariadb.com/downloads/#connectors>
 「Product」を「Java 8 Connector」に変更。
「Download」をclick。
→mariadb-java-client-2.7.2.jarが落ちてくる。
落ちてきたjarをWEB-INF/lib/に置く。
 <https://mariadb.com/kb/en/about-mariadb-connector-j/> でドライバーの使い方を良く把握する。


## 独自業務Exceptionの作成

参考:Why, When and How to Implement Custom Exceptions in Java
 <https://stackify.com/java-custom-exceptions/>

JavaはExceptionを無視できない言語である。

ある程度大きなJavaプログラムで標準例外をそのまま出すのは良くない。
例えばjava.util.IllegalFormatExceptionが出たときに、「どのファイルのどこがIllegalなのか」が
不明だとつらい。

また、DAO(Data Access Object)クラスがSQLExceptionを出すのも良くない。
DAOを使う側が欲しいのはデータである。
DAOを使うプログラマーの要求は「データくれ!」である。
Dataの実態がSQLを使うRDBなのか、ファイルなのか、あるいは他の物なのかは、
使う側にとっては2次的な情報である。
仮に設計変更でデータのありかが、SQLでなく、単なるファイルになったとする。
SQLExceptionをDAOがそのまま外部に見せていたら、
そのDAOを使用しているプログラム全体がExceptionのロジックを変更しなくてはいけない。
従って、何らかの理由でデータが渡せないときには
「データをお渡しできません」が1次的なエラーの情報で、その理由は2次的な情報である。

このようにプログラム・プロジェクト単位で目的が達成できないこと、
その理由が何らかの体系化されるはずである。
その体系を表現するため業務例外を作成する必要が出てくる。
言語標準の例外を業務の例外に変換する作業を「例外翻訳」という。

どうしたら良いかわからない、ここで例外が発生してもすることが無い等の理由で、
例外を無視するプログラムを絶対に書いてはいけない。
さもなくば意味不明なバグが長期間にわたって取れないままになる。

## トランザクション処理

 <https://www.techscore.com/tech/Java/JavaEE/JDBC/3/>

