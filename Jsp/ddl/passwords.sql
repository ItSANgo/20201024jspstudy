CREATE TABLE passwords (
    userId VARCHAR(191) PRIMARY KEY,
	password VARCHAR(512) NOT NULL,
	name VARCHAR(1024),
	roleLevel INTEGER NOT NULL, -- 管理者は1, 一般ユーザーは0
	userNumber INTEGER NOT NULL
);