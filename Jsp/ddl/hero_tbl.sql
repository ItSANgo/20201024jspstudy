﻿create table hero_tbl (
id varchar(10) not null,
class_code varchar(10),
name varchar(20),
level int,
max_hp int default 1 not null,
now_hp int default 1 not null,
max_mp int not null,
now_mp int not null,
atk int not null,
def int not null,
weapon_id int,
exp int not null,
primary key(id)
);

